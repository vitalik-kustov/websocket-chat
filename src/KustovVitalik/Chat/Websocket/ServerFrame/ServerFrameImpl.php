<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ServerFrame;


/**
 * Class ServerFrameImpl
 * @package KustovVitalik\Chat\Websocket\ServerFrame
 */
class ServerFrameImpl implements ServerFrame
{
    /**
     * @var string
     */
    private $command;

    /**
     * @var array
     */
    private $data;

    /**
     * ServerFrameImpl constructor.
     *
     * @param string $command
     * @param array $data
     */
    public function __construct($command = null, array $data = [])
    {
        $this->command = $command;
        $this->data = $data;
    }


    /**
     * @param string $command
     * @param array $data
     *
     * @return mixed
     */
    public static function create($command = null, array $data = [])
    {
        return new ServerFrameImpl($command, $data);
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}