<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ServerFrame;


/**
 * Interface ServerFrame
 * @package KustovVitalik\Chat\Websocket\ServerFrame
 */
interface ServerFrame
{
    /**
     * @param string $command
     * @param array $data
     *
     * @return mixed
     */
    public static function create($command, array $data);

    /**
     * @return string
     */
    public function getCommand();

    /**
     * @return array
     */
    public function getData();
}