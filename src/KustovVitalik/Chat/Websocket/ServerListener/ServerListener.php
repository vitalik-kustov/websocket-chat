<?php
/**
 * Created by PhpStorm.
 * Date: 18.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ServerListener;

use KustovVitalik\Chat\Websocket\ClientFrame\CloseClientFrame;
use KustovVitalik\Chat\Websocket\ClientFrame\MessageClientFrame;
use KustovVitalik\Chat\Websocket\ClientFrame\OpenClientFrame;
use KustovVitalik\Chat\Websocket\Server\Server;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrame;


/**
 * Interface ServerListener
 * @package KustovVitalik\Chat\Websocket
 */
interface ServerListener
{
    /**
     * @param OpenClientFrame $clientFrame
     *
     * @return ServerFrame
     */
    public function onOpen(OpenClientFrame $clientFrame);

    /**
     * @param CloseClientFrame $clientFrame
     *
     * @return ServerFrame
     */
    public function onClose(CloseClientFrame $clientFrame);

    /**
     * @param MessageClientFrame $clientFrame
     *
     * @return ServerFrame
     */
    public function onMessage(MessageClientFrame $clientFrame);

    /**
     * @return Server $server
     */
    public function getServer();

    /**
     * @param Server $server
     *
     * @return void
     */
    public function setServer(Server $server);
}