<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ServerListener;

use KustovVitalik\Chat\Websocket\ClientFrame\ChatClientFrame;
use KustovVitalik\Chat\Websocket\ClientFrame\CloseClientFrame;
use KustovVitalik\Chat\Websocket\ClientFrame\MessageClientFrame;
use KustovVitalik\Chat\Websocket\ClientFrame\OpenClientFrame;
use KustovVitalik\Chat\Websocket\Commands\Command;
use KustovVitalik\Chat\Websocket\Commands\CommandResolver;
use KustovVitalik\Chat\Websocket\Commands\CommandsAware;
use KustovVitalik\Chat\Websocket\Server\Server;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrame;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrameImpl;
use Psr\Log\LoggerInterface;


/**
 * {@inheritDoc}
 */
class ChatCommandsServerListener implements ServerListener, CommandsAware
{

    /**
     * @var Server
     */
    private $server;

    /**
     * @var Command[]|\SplObjectStorage
     */
    private $commands;

    /**
     * @var CommandResolver
     */
    private $commandResolver;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ChatServerListener constructor.
     *
     * @param CommandResolver $resolver
     */
    public function __construct(CommandResolver $resolver)
    {
        $this->commands = new \SplObjectStorage();
        $this->commandResolver = $resolver;
    }

    /**
     * @param OpenClientFrame $clientFrame
     *
     * @return ServerFrame
     */
    public function onOpen(OpenClientFrame $clientFrame)
    {
        return ServerFrameImpl::create('Open');
    }

    /**
     * @param CloseClientFrame $clientFrame
     *
     * @return ServerFrame
     */
    public function onClose(CloseClientFrame $clientFrame)
    {
        return ServerFrameImpl::create('Close');
    }


    /**
     * @param MessageClientFrame $clientFrame
     *
     * @return ServerFrame
     */
    public function onMessage(MessageClientFrame $clientFrame)
    {
        $command = $this->commandResolver->resolveCommand($clientFrame, $this->commands);
        if ($command instanceof Command) {
            $this->logger->info('ChatServerListener: Run command '.$command->getName());
            return $command->run($clientFrame);
        } else {
            $this->logger->warning('Command not found');
            return ServerFrameImpl::create('Warning');
        }
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param Server $server
     */
    public function setServer(Server $server)
    {
        $this->server = $server;
    }


    /**
     * @param Command $command
     *
     * @return mixed
     */
    public function registerCommand(Command $command)
    {
        if (!$this->commands->contains($command)) {
            $this->commands->attach($command);
        }
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}