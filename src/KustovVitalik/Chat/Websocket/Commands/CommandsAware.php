<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Commands;


/**
 * Interface CommandsAware
 * @package KustovVitalik\Chat\Websocket
 */
interface CommandsAware
{
    /**
     * @param Command $command
     *
     * @return mixed
     */
    public function registerCommand(Command $command);
}