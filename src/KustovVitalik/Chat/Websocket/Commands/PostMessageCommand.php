<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Commands;


use KustovVitalik\Chat\Entity\Message;
use KustovVitalik\Chat\Entity\MessageRepository;
use KustovVitalik\Chat\Entity\User;
use KustovVitalik\Chat\Entity\UserRepository;
use KustovVitalik\Chat\Websocket\ClientFrame\MessageClientFrame;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrame;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrameImpl;

/**
 * Class PostMessageCommand
 * @package KustovVitalik\Chat\Websocket\Commands
 */
class PostMessageCommand implements Command
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * PostMessageCommand constructor.
     *
     * @param UserRepository $userRepository
     * @param MessageRepository $messageRepository
     */
    public function __construct(
        UserRepository $userRepository,
        MessageRepository $messageRepository
    ) {
        $this->userRepository = $userRepository;
        $this->messageRepository = $messageRepository;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return Commands::POST_MESSAGE;
    }

    /**
     * @param MessageClientFrame $frame
     *
     * @return ServerFrame
     */
    public function run(MessageClientFrame $frame)
    {
        $userName = $frame->getData()['user'];

        $user = $this->getUser($userName);
        $message = $this->createMessage($user, $frame->getData());

        return ServerFrameImpl::create(
            Commands::POST_MESSAGE,
            [
                'user' => $message->getOwner()
                    ->getName(),
                'messageText' => $message->getText(),
                'createdAt' => $message->getCreatedAt()
                    ->format('Y.m.d H:i:s'),
                'files' => $message->getFiles(),
                'links' => $message->getLinks(),
                'youtubes' => $message->getYoutubes(),
                'messageId' => $message->getId(),
                'likes' => 0,
            ]
        );
    }

    /**
     * @param $userName
     *
     * @return User|null|object
     */
    private function getUser($userName)
    {
        $user = $this->userRepository->find($userName);
        if (!$user) {
            $user = new User();
            $user->setId($userName)
                ->setName($userName);
            $this->userRepository->save($user);

            return $this->userRepository->find($userName);
        }

        return $user;
    }

    /**
     * @param User $user
     * @param $data
     *
     * @return Message
     */
    private function createMessage(User $user, $data)
    {
        $message = new Message();
        $message->setOwner($user)
            ->setText($data['message'])
            ->setYoutubes($data['youtubes'])
            ->setLinks($data['links'])
            ->setFiles($data['files']);
        $this->messageRepository->save($message);

        return $message;
    }
}