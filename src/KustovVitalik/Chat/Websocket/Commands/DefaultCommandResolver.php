<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Commands;


use KustovVitalik\Chat\Websocket\ClientFrame\ChatClientFrame;
use KustovVitalik\Chat\Websocket\ClientFrame\MessageClientFrame;

/**
 * {@inheritDoc}
 */
class DefaultCommandResolver implements CommandResolver
{

    /**
     * @param MessageClientFrame $frame
     * @param \SplObjectStorage $commands
     *
     * @return Command
     */
    public function resolveCommand(MessageClientFrame $frame, $commands)
    {
        foreach ($commands as $command) {
            if ($frame->getCommand() === $command->getName()) {
                return $command;
            }
        }
    }
}