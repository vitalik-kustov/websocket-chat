<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Commands;


use KustovVitalik\Chat\Websocket\ClientFrame\MessageClientFrame;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrame;

/**
 * Interface Command
 * @package KustovVitalik\Chat\Websocket\Commands
 */
interface Command
{

    /**
     * @return string
     */
    public function getName();

    /**
     * @param MessageClientFrame $frame
     *
     * @return ServerFrame
     */
    public function run(MessageClientFrame $frame);
}