<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Commands;


use KustovVitalik\Chat\Entity\Message;
use KustovVitalik\Chat\Entity\MessageRepository;
use KustovVitalik\Chat\Entity\User;
use KustovVitalik\Chat\Entity\UserRepository;
use KustovVitalik\Chat\Websocket\ClientFrame\MessageClientFrame;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrame;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrameImpl;

/**
 * Class DeleteMessageCommand
 * @package KustovVitalik\Chat\Websocket\Commands
 */
class DeleteMessageCommand implements Command
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * DeleteMessageCommand constructor.
     *
     * @param UserRepository $userRepository
     * @param MessageRepository $messageRepository
     */
    public function __construct(UserRepository $userRepository, MessageRepository $messageRepository)
    {
        $this->userRepository = $userRepository;
        $this->messageRepository = $messageRepository;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Commands::DELETE_MESSAGE;
    }

    /**
     * @param MessageClientFrame $frame
     *
     * @return ServerFrame
     */
    public function run(MessageClientFrame $frame)
    {
        $messageId = (int) $frame->getData()['messageId'];
        $userName = $frame->getData()['user'];

        /** @var Message $message */
        $message = $this->messageRepository->find($messageId);
        /** @var User $user */
        $user = $this->userRepository->find($userName);

        if ($message && $user && $message->getOwner()->getId() === $user->getId()) {
            $this->messageRepository->delete($message);

            return ServerFrameImpl::create(Commands::DELETE_MESSAGE, [
                'status' => 'Ok',
                'messageId' => $messageId
            ]);
        }

        return ServerFrameImpl::create(Commands::DELETE_MESSAGE, [
            'status' => 'Error',
            'messageId' => $messageId
        ]);
    }
}