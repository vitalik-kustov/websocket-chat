<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Commands;


use KustovVitalik\Chat\Websocket\ClientFrame\MessageClientFrame;

/**
 * Interface CommandResolver
 * @package KustovVitalik\Chat\Websocket\Commands
 */
interface CommandResolver
{
    /**
     * @param MessageClientFrame $frame
     * @param $commands
     *
     * @return Command
     */
    public function resolveCommand(MessageClientFrame $frame, $commands);
}