<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Commands;


use KustovVitalik\Chat\Entity\Message;
use KustovVitalik\Chat\Entity\MessageLike;
use KustovVitalik\Chat\Entity\MessageLikeRepository;
use KustovVitalik\Chat\Entity\MessageRepository;
use KustovVitalik\Chat\Entity\User;
use KustovVitalik\Chat\Entity\UserRepository;
use KustovVitalik\Chat\Websocket\ClientFrame\MessageClientFrame;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrame;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrameImpl;

/**
 * Class LileMessageCommand
 * @package KustovVitalik\Chat\Websocket\Commands
 */
class LikeMessageCommand implements Command
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * @var MessageLikeRepository
     */
    private $likeRepository;

    /**
     * LikeMessageCommand constructor.
     *
     * @param UserRepository $userRepository
     * @param MessageRepository $messageRepository
     * @param MessageLikeRepository $likeRepository
     */
    public function __construct(
        UserRepository $userRepository,
        MessageRepository $messageRepository,
        MessageLikeRepository $likeRepository
    ) {
        $this->userRepository = $userRepository;
        $this->messageRepository = $messageRepository;
        $this->likeRepository = $likeRepository;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return Commands::LIKE_MESSAGE;
    }

    /**
     * @param MessageClientFrame $frame
     *
     * @return ServerFrame
     */
    public function run(MessageClientFrame $frame)
    {
        $messageId = $frame->getData()['messageId'];
        $userName = $frame->getData()['user'];

        $user = $this->getUser($userName);
        /** @var Message $message */
        $message = $this->messageRepository->find($messageId);

        if ($user && $message) {
            $like = $this->likeRepository->findOneBy(
                [
                    'user' => $user,
                    'message' => $message,
                ]
            );
            if (!$like) {
                $like = new MessageLike();
                $like->setUser($user);
                $like->setMessage($message);
                $this->messageRepository->save($message);

                return ServerFrameImpl::create(
                    Commands::LIKE_MESSAGE,
                    [
                        'messageId' => $message->getId(),
                        'likes' => $message->getLikes()
                            ->count(),
                        'info' => 'Added',
                        'user' => $user->getName(),
                    ]
                );
            }

            return ServerFrameImpl::create(
                Commands::LIKE_MESSAGE,
                [
                    'messageId' => $message->getId(),
                    'likes' => $message->getLikes()
                        ->count(),
                    'info' => 'AlreadyExist',
                    'user' => $user->getName(),
                ]
            );
        }

        return ServerFrameImpl::create(
            Commands::LIKE_MESSAGE,
            [
                'info' => 'Error',
            ]
        );

    }

    /**
     * @param $userName
     *
     * @return User|null|object
     */
    private function getUser($userName)
    {
        $user = $this->userRepository->find($userName);
        if (!$user) {
            $user = new User();
            $user->setId($userName)
                ->setName($userName);
            $this->userRepository->save($user);
        }

        return $user;
    }
}