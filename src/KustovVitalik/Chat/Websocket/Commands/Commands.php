<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Commands;


/**
 * Interface Commands
 * @package KustovVitalik\Chat\Websocket\Commands
 */
interface Commands
{
    /**
     * Post message
     */
    const POST_MESSAGE = 'postMessage';
    /**
     * Delete message
     */
    const DELETE_MESSAGE = 'deleteMessage';
    /**
     * Like message
     */
    const LIKE_MESSAGE = 'likeMessage';
}