<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ClientFrame;


/**
 * Interface OpenClientFrame
 * @package KustovVitalik\Chat\Websocket\ClientFrame
 */
interface OpenClientFrame
{
    /**
     * @return mixed
     */
    public function getClient();

    /**
     * @return mixed
     */
    public function getInfo();
}