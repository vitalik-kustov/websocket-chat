<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ClientFrame;


/**
 * Class OpenClientFrameImpl
 * @package KustovVitalik\Chat\Websocket\ClientFrame
 */
class OpenClientFrameImpl implements OpenClientFrame
{

    /**
     * @var mixed
     */
    private $client;

    /**
     * @var mixed
     */
    private $info;

    /**
     * OpenClientFrameImpl constructor.
     *
     * @param $client
     * @param $info
     */
    public function __construct($client, $info)
    {
        $this->client = $client;
        $this->info = $info;
    }


    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }
}