<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ClientFrame;


/**
 * Interface ClientFrame
 * @package KustovVitalik\Chat\Websocket\ClientFrame
 */
interface MessageClientFrame
{

    /**
     * @param array $data
     *
     * @return MessageClientFrame
     */
    public static function create(array $data);

    /**
     * @return string
     */
    public function getCommand();

    /**
     * @return array
     */
    public function getData();

    /**
     * @return mixed
     */
    public function getClient();
}