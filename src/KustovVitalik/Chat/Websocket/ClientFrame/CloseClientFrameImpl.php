<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ClientFrame;


/**
 * Class CloseClientFrameImpl
 * @package KustovVitalik\Chat\Websocket\ClientFrame
 */
class CloseClientFrameImpl implements CloseClientFrame
{
    /**
     * @var mixed
     */
    private $client;

    /**
     * CloseClientFrameImpl constructor.
     *
     * @param $client
     */
    public function __construct($client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }
}