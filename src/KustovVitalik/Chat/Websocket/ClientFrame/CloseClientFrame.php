<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ClientFrame;


/**
 * Interface CloseClientFrame
 * @package KustovVitalik\Chat\Websocket\ClientFrame
 */
interface CloseClientFrame
{
    /**
     * @return mixed
     */
    public function getClient();
}