<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\ClientFrame;


/**
 * Class MessageClientFrameImpl
 * @package KustovVitalik\Chat\Websocket\ClientFrame
 */
class MessageClientFrameImpl implements MessageClientFrame
{
    /**
     * @var string
     */
    private $command;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @var mixed
     */
    private $client;

    /**
     * @param $client
     * @param $command
     * @param $data
     */
    public function __construct($client, $command, $data)
    {
        $this->command = $command;
        $this->data = $data;
        $this->client = $client;
    }


    /**
     * @param array $data
     *
     * @return MessageClientFrame
     */
    public static function create(array $data)
    {
        return new MessageClientFrameImpl(
            $data['client'],
            $data['command'],
            $data['data']
        );
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }
}