<?php
/**
 * Created by PhpStorm.
 * Date: 18.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Server;

use KustovVitalik\Chat\Websocket\ClientFrame\CloseClientFrameImpl;
use KustovVitalik\Chat\Websocket\ClientFrame\MessageClientFrameImpl;
use KustovVitalik\Chat\Websocket\ClientFrame\OpenClientFrameImpl;
use KustovVitalik\Chat\Websocket\ServerFrame\ServerFrame;
use KustovVitalik\Chat\Websocket\ServerListener\ServerListener;
use Psr\Log\LoggerInterface;

/**
 * Class Server
 * @package KustovVitalik\Chat\Websocket
 */
class ChatServer implements Server
{
    /**
     * @var array
     */
    private $clients = [];
    /**
     * @var resource
     */
    private $server;
    /**
     * @var array
     */
    private $handshakes = [];

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var \KustovVitalik\Chat\Websocket\ServerListener\ServerListener[]
     */
    private $listeners;

    /**
     * @param array $config
     * @param LoggerInterface $logger
     *
     * @throws \Exception
     */
    public function __construct(array $config, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $server = stream_socket_server(
            "tcp://{$config['host']}:{$config['port']}",
            $errorNumber,
            $errorString
        );

        if (!$server) {
            $this->logger->error("error: stream_socket_server: $errorString ($errorNumber)");
            throw new \Exception('Cannot create socket server.');
        }
        $this->server = $server;
        $this->listeners = new \SplObjectStorage();

    }

    /**
     * @param \KustovVitalik\Chat\Websocket\ServerListener\ServerListener $listener
     */
    public function addListener(ServerListener $listener)
    {
        if (!$this->listeners->contains($listener)) {
            $this->listeners->attach($listener);
            $listener->setServer($this);
        }
    }

    public function stop()
    {
        @fclose($this->server);
        $this->logger->info('Server stopped');
    }

    public function start()
    {
        $this->logger->info('Server start');
        while (true) {
            $read = $this->clients;
            $read[] = $this->server;
            $write = [];
            if ($this->handshakes) {
                foreach ($this->handshakes as $clientId => $clientInfo) {
                    if ($clientInfo) {
                        $write[] = $this->clients[$clientId];
                    }
                }
            }

            stream_select($read, $write, $except, null);

            if (in_array($this->server, $read)) {
                if ($client = stream_socket_accept($this->server, -1)) {
                    $this->clients[(int)$client] = $client;
                    $this->handshakes[(int)$client] = [];
                }
                unset($read[array_search($this->server, $read)]);
            }

            if ($read) {
                foreach ($read as $client) {
                    if (array_key_exists((int)$client, $this->handshakes)) {
                        if ($this->handshakes[(int)$client]) {
                            continue;
                        }

                        if (!$this->handshake($client)) {
                            unset($this->clients[(int)$client], $this->handshakes[(int)$client]);
                            @fclose($client);
                        }
                    } else {
                        $data = fread($client, 8192);

                        if (mb_strlen($data) === 0) {
                            unset($this->clients[(int)$client], $this->handshakes[(int)$client]);
                            $this->onClose($client);
                            @fclose($client);
                            continue;
                        }

                        $this->onMessage($client, $data);
                    }
                }
            }

            if ($write) {
                foreach ($write as $client) {
                    if (!$this->handshakes[(int)$client]) {
                        continue;
                    }
                    $info = $this->handshake($client);
                    $this->onOpen($client, $info);
                }
            }
        }
    }

    /**
     * @param $client
     *
     * @return bool
     */
    protected function handshake($client)
    {
        $key = $this->handshakes[(int)$client];

        if (!$key) {
            //считываем загаловки из соединения
            $headers = fread($client, 8192);

            preg_match("/Sec-WebSocket-Key: (.*)\r\n/", $headers, $match);

            if (empty($match[1])) {
                return false;
            }

            $key = $match[1];

            $this->handshakes[(int)$client] = $key;
        } else {
            //отправляем заголовок согласно протоколу вебсокета
            $SecWebSocketAccept = base64_encode(pack('H*', sha1($key.'258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
            $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n".
                "Upgrade: websocket\r\n".
                "Connection: Upgrade\r\n".
                "Sec-WebSocket-Accept:$SecWebSocketAccept\r\n\r\n";
            fwrite($client, $upgrade);
            unset($this->handshakes[(int)$client]);
        }

        return $key;
    }

    /**
     * @param $payload
     * @param string $type
     * @param bool|false $masked
     *
     * @return string
     */
    protected function encode($payload, $type = 'text', $masked = false)
    {
        $frameHead = [];
        $payloadLength = strlen($payload);

        switch ($type) {
            case 'text':
                // first byte indicates FIN, Text-Frame (10000001):
                $frameHead[0] = 129;
                break;

            case 'close':
                // first byte indicates FIN, Close Frame(10001000):
                $frameHead[0] = 136;
                break;

            case 'ping':
                // first byte indicates FIN, Ping frame (10001001):
                $frameHead[0] = 137;
                break;

            case 'pong':
                // first byte indicates FIN, Pong frame (10001010):
                $frameHead[0] = 138;
                break;
        }

        // set mask and payload length (using 1, 3 or 9 bytes)
        if ($payloadLength > 65535) {
            $payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
            $frameHead[1] = ($masked === true) ? 255 : 127;
            for ($i = 0; $i < 8; $i++) {
                $frameHead[$i + 2] = bindec($payloadLengthBin[$i]);
            }
            // most significant bit MUST be 0
            if ($frameHead[2] > 127) {
                return [
                    'type' => '',
                    'payload' => '',
                    'error' => 'frame too large (1004)',
                ];
            }
        } elseif ($payloadLength > 125) {
            $payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
            $frameHead[1] = ($masked === true) ? 254 : 126;
            $frameHead[2] = bindec($payloadLengthBin[0]);
            $frameHead[3] = bindec($payloadLengthBin[1]);
        } else {
            $frameHead[1] = ($masked === true) ? $payloadLength + 128 : $payloadLength;
        }

        // convert frame-head to string:
        foreach (array_keys($frameHead) as $i) {
            $frameHead[$i] = chr($frameHead[$i]);
        }
        if ($masked === true) {
            // generate a random mask:
            $mask = [];
            for ($i = 0; $i < 4; $i++) {
                $mask[$i] = chr(rand(0, 255));
            }

            $frameHead = array_merge($frameHead, $mask);
        }
        $frame = implode('', $frameHead);

        // append payload to frame:
        for ($i = 0; $i < $payloadLength; $i++) {
            $frame .= ($masked === true) ? $payload[$i] ^ $mask[$i % 4] : $payload[$i];
        }

        return $frame;
    }

    /**
     * @param $data
     *
     * @return array|bool
     */
    protected function decode($data)
    {
        $unmaskedPayload = '';
        $decodedData = [];

        // estimate frame type:
        $firstByteBinary = sprintf('%08b', ord($data[0]));
        $secondByteBinary = sprintf('%08b', ord($data[1]));
        $opcode = bindec(substr($firstByteBinary, 4, 4));
        $isMasked = ($secondByteBinary[0] == '1') ? true : false;
        $payloadLength = ord($data[1]) & 127;

        // unmasked frame is received:
        if (!$isMasked) {
            return [
                'type' => '',
                'payload' => '',
                'error' => 'protocol error (1002)',
            ];
        }

        switch ($opcode) {
            // text frame:
            case 1:
                $decodedData['type'] = 'text';
                break;

            case 2:
                $decodedData['type'] = 'binary';
                break;

            // connection close frame:
            case 8:
                $decodedData['type'] = 'close';
                break;

            // ping frame:
            case 9:
                $decodedData['type'] = 'ping';
                break;

            // pong frame:
            case 10:
                $decodedData['type'] = 'pong';
                break;

            default:
                return [
                    'type' => '',
                    'payload' => '',
                    'error' => 'unknown opcode (1003)',
                ];
        }

        if ($payloadLength === 126) {
            $mask = substr($data, 4, 4);
            $payloadOffset = 8;
            $dataLength = bindec(sprintf('%08b', ord($data[2])).sprintf('%08b', ord($data[3]))) + $payloadOffset;
        } elseif ($payloadLength === 127) {
            $mask = substr($data, 10, 4);
            $payloadOffset = 14;
            $tmp = '';
            for ($i = 0; $i < 8; $i++) {
                $tmp .= sprintf('%08b', ord($data[$i + 2]));
            }
            $dataLength = bindec($tmp) + $payloadOffset;
            unset($tmp);
        } else {
            $mask = substr($data, 2, 4);
            $payloadOffset = 6;
            $dataLength = $payloadLength + $payloadOffset;
        }

        /**
         * We have to check for large frames here. socket_recv cuts at 1024 bytes
         * so if websocket-frame is > 1024 bytes we have to wait until whole
         * data is transferd.
         */
        if (strlen($data) < $dataLength) {
            return false;
        }

        if ($isMasked) {
            for ($i = $payloadOffset; $i < $dataLength; $i++) {
                $j = $i - $payloadOffset;
                if (isset($data[$i])) {
                    $unmaskedPayload .= $data[$i] ^ $mask[$j % 4];
                }
            }
            $decodedData['payload'] = $unmaskedPayload;
        } else {
            $payloadOffset = $payloadOffset - 4;
            $decodedData['payload'] = substr($data, $payloadOffset);
        }

        return $decodedData;
    }

    /**
     * @param $client
     * @param $info
     */
    protected function onOpen($client, $info)
    {
        $this->logger->info('Server open event', [
            'client' => $client,
            'info' => $info,
        ]);

        $frame = new OpenClientFrameImpl($client, $info);
        foreach ($this->listeners as $listener) {
            $this->send($listener->onOpen($frame));
        }
    }

    /**
     * @param $client
     */
    protected function onClose($client)
    {
        $this->logger->info('Server close event', [
            'client' => $client,
        ]);
        $frame = new CloseClientFrameImpl($client);
        foreach ($this->listeners as $listener) {
            $this->send($listener->onClose($frame));
        }
    }

    /**
     * @param $client
     * @param $data
     */
    protected function onMessage($client, $data)
    {
        $this->logger->info('Server message event', [
            'client' => $client,
            'data' => $data,
        ]);
        $data = $this->decode($data);
        if (!$data['payload']) {
            return;
        }
        if (!mb_check_encoding($data['payload'], 'utf-8')) {
            return;
        }

        $clientMessage = json_decode($data['payload'], true);
        $frame = MessageClientFrameImpl::create([
            'client' => $client,
            'command' => $clientMessage['command'],
            'data' => $clientMessage['data']
        ]);

        foreach ($this->listeners as $listener) {
            $this->send($listener->onMessage($frame));
        }
    }

    /**
     * @param ServerFrame $serverFrame
     */
    private function send(ServerFrame $serverFrame)
    {
        $this->logger->info('Memory usage: '.memory_get_usage().', peak: '.memory_get_peak_usage());

        $response = json_encode([
            'command' => $serverFrame->getCommand(),
            'data' => $serverFrame->getData()
        ]);

        $data = $this->encode($response);
        $write = $this->clients;
        if (stream_select($read, $write, $except, 0)) {
            foreach ($write as $client) {
                @fwrite($client, $data);
            }
        }
    }

}