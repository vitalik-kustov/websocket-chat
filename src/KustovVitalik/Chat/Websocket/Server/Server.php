<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Websocket\Server;


use KustovVitalik\Chat\Websocket\ServerListener\ServerListener;

/**
 * Interface Server
 * @package KustovVitalik\Chat\Websocket
 */
interface Server
{
    /**
     * @return void
     */
    public function start();

    /**
     * @return void
     */
    public function stop();

    /**
     * @param ServerListener $listener
     *
     * @return void
     */
    public function addListener(ServerListener $listener);
}