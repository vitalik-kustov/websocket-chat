<?php
/**
 * Created by PhpStorm.
 * Date: 13.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader as DiYamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Router;

/**
 * Class Chat
 * @package KustovVitalik\Chat
 */
class Chat
{

	/**
	 * @var ContainerInterface
	 */
	public $container;

	/**
	 * Chat constructor.
	 */
	public function __construct()
	{
		$this->container = new ContainerBuilder();
		$this->container->setParameter('app_dir', APP_DIR);
		$this->container->set('service_container', $this->container);
		$containerLoader = new DiYamlFileLoader(
			$this->container, new FileLocator(APP_DIR . 'config')
		);
		$containerLoader->load('services.yml');
		$this->container->compile();
	}

	/**
	 * Run the chat application
	 *
	 * @return void
	 * @throws \Exception
	 */
	public function run()
	{
		$request = Request::createFromGlobals();

		$matcher = new Router(
			new YamlFileLoader(
				new FileLocator(
					$this->container->getParameter('config_dir')
				)
			),
			'routes.yml'
		);

		$resolver = new ControllerResolver();

		try {
			$request->attributes->add($matcher->matchRequest($request));
			$controller = $resolver->getController($request);
			$arguments = $resolver->getArguments($request, $controller);

			list($class,) = $controller;
			if ($class instanceof ContainerAwareInterface) {
				$class->setContainer($this->container);
			}

			$response = call_user_func_array($controller, $arguments);
		} catch (ResourceNotFoundException $e) {
			$response = new Response('Page not found.', Response::HTTP_NOT_FOUND);
		} catch (MethodNotAllowedException $e) {
			$response = new Response('Method not allowed.', Response::HTTP_METHOD_NOT_ALLOWED);
		} catch (\Exception $e) {
			$env = $this->container->getParameter('env');
			if ($env === 'dev') {
				throw $e;
			} elseif ($env === 'prod') {
				$response = new Response('Internal server error.', Response::HTTP_INTERNAL_SERVER_ERROR);
			}

		}

		$response->send();
	}
}