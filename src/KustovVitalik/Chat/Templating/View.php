<?php
/**
 * Created by PhpStorm.
 * Date: 15.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Templating;


class View
{
	/**
	 * Path to views directory
	 */
	private $viewsPath;

	/**
	 * View constructor.
	 *
	 * @param null $viewsPath
	 */
	public function __construct($viewsPath = null)
	{
		if ($viewsPath === null) {
			$viewsPath = APP_DIR . 'app/views/';
		}
		$this->viewsPath = $viewsPath;
	}

	/**
	 * @return static
	 */
	public static function factory()
	{
		return new static();
	}

	/**
	 * @param string $view
	 * @param array $params
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function render($view, array $params = [])
	{
		$file = $this->viewsPath . $view . '.php';
		if (file_exists($file)) {
			ob_start();
			extract($params);
			include_once $file;
			return ob_get_clean();
		} else {
			throw new \Exception(sprintf('File for view %s does not exist.', $view));
		}
	}
}