<?php
/**
 * Created by PhpStorm.
 * Date: 14.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;


/**
 * Class Message
 * @Entity(repositoryClass="KustovVitalik\Chat\Entity\MessageRepository")
 * @Table(name="messages")
 * @package KustovVitalik\Chat\Entity
 */
class Message
{
    /**
     * @var int
     * @Id()
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     * @ManyToOne(targetEntity="KustovVitalik\Chat\Entity\User")
     * @JoinColumn(name="user_id", nullable=false, onDelete="CASCADE")
     */
    private $owner;

    /**
     * @var string
     * @Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var array
     * @Column(name="links", type="array", nullable=true)
     */
    private $links;

    /**
     * @var array
     * @Column(name="files", type="array", nullable=true)
     */
    private $files;

    /**
     * @var array
     * @Column(name="youtubes", type="array", nullable=true)
     */
    private $youtubes;

    /**
     * @var MessageLike[]|Collection
     * @OneToMany(targetEntity="MessageLike", mappedBy="message", cascade={"persist"})
     */
    private $likes;

    /**
     * @Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Message constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->likes = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     *
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param array $links
     *
     * @return $this
     */
    public function setLinks($links)
    {
        $this->links = $links;

        return $this;
    }

    /**
     * @return array
     */
    public function getYoutubes()
    {
        return $this->youtubes;
    }

    /**
     * @param array $youtubes
     *
     * @return $this
     */
    public function setYoutubes($youtubes)
    {
        $this->youtubes = $youtubes;

        return $this;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param array $files
     *
     * @return $this
     */
    public function setFiles($files)
    {
        $this->files = $files;

        return $this;
    }

    /**
     * @return Collection|MessageLike[]
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param MessageLike $like
     *
     * @return $this
     */
    public function addLike(MessageLike $like)
    {
        $this->likes[] = $like;

        return $this;
    }

    /**
     * @param MessageLike $like
     *
     * @return $this
     */
    public function removeLike(MessageLike $like)
    {
        $this->likes->removeElement($like);

        return $this;
    }

}