<?php
/**
 * Created by PhpStorm.
 * Date: 14.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;


/**
 * Class User
 * @Entity(repositoryClass="KustovVitalik\Chat\Entity\UserRepository")
 * @Table(name="users")
 * @package KustovVitalik\Chat\Entity
 */
class User
{
    /**
     * @var int
     * @Id()
     * @Column(type="string", length=255)
     */
    private $id;

    /**
     * @var string
     * @Column(name="name", type="string", length=255, nullable=true, unique=true)
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }


}