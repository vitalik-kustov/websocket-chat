<?php
/**
 * Created by PhpStorm.
 * Date: 19.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;


/**
 * Class MessageLike
 * @Entity(repositoryClass="KustovVitalik\Chat\Entity\MessageLikeRepository")
 * @Table(name="message_likes")
 * @package KustovVitalik\Chat\Entity
 */
class MessageLike
{
    /**
     * @var int
     * @Id()
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Message
     * @ManyToOne(targetEntity="Message", inversedBy="likes", cascade={"persist"})
     * @JoinColumn(name="message_id", nullable=false, onDelete="CASCADE")
     */
    private $message;

    /**
     * @var User
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param Message $message
     *
     * @return $this
     */
    public function setMessage(Message $message)
    {
        $this->message = $message;

        if (!$message->getLikes()
            ->contains($this)
        ) {
            $message->addLike($this);
        }

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }


}