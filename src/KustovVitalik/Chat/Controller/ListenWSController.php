<?php
/**
 * Created by PhpStorm.
 * Date: 18.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Controller;


/**
 * Class ListenWSController
 * @package KustovVitalik\Chat\Controller
 */
class ListenWSController extends ContainerAware
{
    public function indexAction()
    {
        $this->container->get('ws_server')
            ->start();
    }
}