<?php
/**
 * Created by PhpStorm.
 * Date: 14.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\Chat\Controller;


use KustovVitalik\Chat\Templating\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package KustovVitalik\Chat\Controller
 */
class DefaultController extends ContainerAware
{
    /**
     * @return Response
     * @throws \Exception
     */
    public function indexAction()
    {
        return new Response(
            View::factory()
                ->render('layout', [])
        );
    }
}