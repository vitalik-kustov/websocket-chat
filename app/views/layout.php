<!DOCTYPE html>
<html>
<meta charset="UTF-8"/>
<head>
    <title>Чат</title>
    <style>
        iframe, img {
            max-width: 300px;
            max-height: 300px;
            display: inline-block;
        }
    </style>
</head>
<body>
<h1>Чат</h1>

<div id="form-container">
    <form id="form" action="" method="post">
        <div>
            <textarea id="message" placeholder="Привет парни!" name="message"></textarea>
        </div>

        <div class="form-buttons">
            <button id="add-link">Добавить ссылку</button>
            <input id="add-file" type="file" name="files[]" multiple="multiple" accept="image/jpeg,image/png,image/gif">
            <button id="add-youtube">Добавить YouTube ролик</button>
        </div>

        <input type="submit" value="Отправить"/>

        <div id="attachments-container"></div>
    </form>
</div>
<div>
    <ul id="message-container"></ul>
</div>
<script src="/js/application.js"></script>
</body>
</html>