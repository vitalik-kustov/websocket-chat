class Socket {
    private connectionString:string;

    private socket:WebSocket;

    private onMessageHandler:(ev:MessageEvent) => any;

    constructor(host:string, port:string) {
        this.connectionString = 'ws://' + host + ':' + port;
    }

    public setMessageHandler(handler: (ev:MessageEvent) => any):void {
        this.onMessageHandler = handler;
    }

    public close():void {
        this.socket.close();
    }

    public connect():void {
        if (!this.onMessageHandler) {
            throw new Error('Message handler is not defined');
        }
        this.socket = new WebSocket(this.connectionString);
        if (this.socket) {
            this.socket.onmessage = this.onMessageHandler;
            this.socket.onopen = function() {
                console.log('Соединение установлено');
            };
            this.socket.onclose = function(e:CloseEvent) {
                if (e.wasClean) {
                    console.log('Соединение закрыто чисто');
                } else {
                    console.log('Обрыв соединения');
                }
                console.log('Код: ' + event.code + ', причина: ' + event.reason);
            };
            this.socket.onerror = function(e:ErrorEvent) {
                console.log('Ошибка: ' + e.error);
            }
        }
    }

    public send(data:string|ArrayBuffer|Blob) {
        this.socket.send(data);
    }
}



interface Server {
    start():void;
    stop():void;
    addListener(listener:CommandServerListener):void;
}
interface CommandServerListener {
    getServer():Server;
    setServer(server:Server):void;
    registerCommand(command:Command):void;
    onServerMessage(serverMessage:ServerMessage):string|ArrayBuffer|Blob;
}
interface Command {
    getName():string;
    run():{};
}
interface ServerMessage {
    getCommand():string;
    getData():any;
}

class ChatServerMessage implements ServerMessage {

    private command:string;
    private data:any;

    constructor(commandName:string, data:any) {
        this.command = commandName;
        this.data = data;
    }

    getCommand():string {
        return undefined;
    }

    getData():any {
        return undefined;
    }

    static fromMessageEvent(ev:MessageEvent):ServerMessage {
        var data = JSON.parse(ev.data);
        return new ChatServerMessage(data.command, data.data);
    }

}

class ChatServer implements Server {

    private listeners:CommandServerListener[];
    private socket:Socket;

    constructor(socket:Socket) {
        this.listeners = [];
        this.socket = socket;
    }

    start():void {
        var that = this;
        this.socket.connect();
        this.socket.setMessageHandler(function(ev:MessageEvent) {
            var serverMessage = ChatServerMessage.fromMessageEvent(ev);
            that.listeners.forEach(function(listener:CommandServerListener) {
                that.socket.send(listener.onServerMessage(serverMessage));
            });
        });
    }

    stop():void {
        this.socket.close();
    }

    addListener(listener:CommandServerListener):void {
        this.listeners.push(listener);
        listener.setServer(this);
    }
}



var socket = new Socket('127.0.0.1', '8000');
var server = new ChatServer(socket);
server.addListener();
try {
    server.start();
} catch (e) {
    server.stop();
    console.log(e);
}