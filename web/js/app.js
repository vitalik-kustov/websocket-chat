;
(function (document, undefined) {
	'use strict';
	Element.prototype.remove = function () {
		this.parentElement.removeChild(this);
	};
	NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
		for (var i = this.length - 1; i >= 0; i--) {
			if (this[i] && this[i].parentElement) {
				this[i].parentElement.removeChild(this[i]);
			}
		}
	};
	var URL_REGEXP = /^(https?:\/\/)?([\w\.]+)\.([a-z]{2,6}\.?)(\/[\w\.]*)*\/?$/;
	var YOUTUBE_REGEXP = /youtube\.com\/embed/;
	var Utils = {
		inArray: function (item, array) {
			var found = false, key;
			for (key in array) {
				if (array[key] == item) {
					found = true;
					break;
				}
			}
			return found;
		}
	};

	var NoConnection = function() {
		alert('Нет подключения к серверу. Чат недоступен.');
		document.querySelector('body').innerHTML = 'Запустите сервер и нажмите на кнопку<br/>';
		var reloadButton = document.createElement('button');
		reloadButton.innerHTML = 'Сервер запущен. Старт!';
		reloadButton.onclick = function(e) {
			e.preventDefault();
			e.stopPropagation();
			window.location = window.location;
		};
		document.querySelector('body').appendChild(reloadButton);
	};

	var User = {
		name: null,
		setName: function (name) {
			User.name = name;
			window.localStorage.setItem('username', name);
		},
		getName: function () {
			if (Utils.inArray(User.name, [null, '', undefined, 'undefined'])) {
				var name = window.localStorage.getItem('username');
				if (!Utils.inArray(name, [null, 'undefined', '', undefined])) {
					User.setName(name);
				} else {
					User.askUser();
				}
			}
			return User.name;
		},
		askUser: function () {
			if (Utils.inArray(User.name, [null, '', 'undefined', undefined])) {
				var username = prompt("Укажите как Вас величать");
				if (!Utils.inArray(username, ['', null, undefined, 'undefined'])) {
					User.setName(username.trim());
				} else {
					User.askUser();
				}
			}
		}
	};
	var Socket = function (host, port, responseHandler) {
		Socket.responseHandler = responseHandler;
		Socket._socket = new WebSocket('ws://' + host + ':' + port);
		Socket._socket.onopen = this.onOpen;
		Socket._socket.onclose = this.onClose;
		Socket._socket.onmessage = this.onMessage;
		Socket._socket.onerror = function (e) {
			console.error(e);
		};
	};
	Socket.prototype.constructor = Socket;
	Socket.prototype.onOpen = function (e) {
		console.log('Connected');
	};
	Socket.prototype.onClose = function (e) {
		NoConnection();
	};
	Socket.prototype.onMessage = function (e) {
		var data = JSON.parse(e.data);
		switch (data.command) {
			case 'postMessage':
				Socket.responseHandler.postMessage(data.data);
				break;
			case 'deleteMessage':
				Socket.responseHandler.deleteMessage(data.data);
				break;
			case 'likeMessage':
				Socket.responseHandler.likeMessage(data.data);
				break;
			case 'Open':
			case 'Close':
			default :
				break;
		}
	};
	Socket.prototype.send = function (data) {
		return Socket._socket.send(data);
	};
	var ClientRequest = function (command, data) {
		this.command = command;
		this.data = data;
	};
	ClientRequest.prototype.toJson = function () {
		return JSON.stringify({
			command: this.command,
			data: this.data
		});
	};
	var ServerFasade = {
		webSocket: null,
		setSocket: function (socket) {
			ServerFasade.webSocket = socket;
		},
		likeMessage: function (data) {
			var request = new ClientRequest('likeMessage', data);
			ServerFasade.webSocket.send(request.toJson());
		},
		postMessage: function (message) {
			var request = new ClientRequest('postMessage', message);
			ServerFasade.webSocket.send(request.toJson());
		},
		deleteMessage: function (data) {
			var request = new ClientRequest('deleteMessage', data);
			ServerFasade.webSocket.send(request.toJson());
		}
	};
	var MessageFormDataContainer = function () {
		this.username = User.getName();
		this.messageText = null;
		this.links = [];
		this.youtubes = [];
		this.files = [];
	};
	MessageFormDataContainer.prototype.toJson = function () {
		return JSON.stringify({
			user: User.getName(),
			message: this.messageText,
			links: this.links,
			youtubes: this.youtubes,
			files: this.files
		});
	};
	MessageFormDataContainer.prototype.clear = function () {
		this.username = User.getName();
		this.messageText = null;
		this.links = [];
		this.youtubes = [];
		this.files = [];
	};
	MessageFormDataContainer.prototype.getMessage = function () {
		return {
			message: this.messageText,
			user: User.getName(),
			links: this.links,
			youtubes: this.youtubes,
			files: this.files
		}
	};
	var LikeMessageFormDataContainer = function (messageId) {
		this.messageId = messageId;
		this.user = User.getName();
	};
	LikeMessageFormDataContainer.prototype.toJson = function () {
		return JSON.stringify({
			user: User.getName(),
			messageId: this.messageId
		});
	};
	var DeleteMessageFormDataContainer = function (messageId) {
		this.messageId = messageId;
		this.user = User.getName();
	};
	DeleteMessageFormDataContainer.prototype.toJson = function () {
		return JSON.stringify({
			user: User.getName(),
			messageId: this.messageId
		});
	};
	var GUI = {
		addMessageLink: function (link) {
			var a = document.createElement('a');
			a.innerHTML = link;
			a.href = link;
			GUI._previewMessageContainer.add(a);
		},
		addYoutubeLink: function (link) {
			var elem = document.createElement('iframe');
			elem.width = 560;
			elem.height = 315;
			elem.src = link;
			elem.frameborder = 0;
			elem.allowfullscreen = null;
			GUI._previewMessageContainer.add(elem);
		},
		addImage: function (image) {
			GUI._previewMessageContainer.add(image);
		},
		crearPreviewMessageContainer: function () {
			GUI._previewMessageContainer.clean();
		},
		clearControls: function () {
			document.getElementById('message').value = null;
			document.getElementById('add-file').value = null;
		},
		_previewMessageContainer: {
			add: function (elem) {
				var elemContainer = document.createElement('div');
				elemContainer.className = 'elem-container';
				elemContainer.appendChild(elem);
				document.getElementById('attachments-container').appendChild(elemContainer);
			},
			clean: function () {
				document.getElementById('attachments-container').innerHTML = null;
			}
		}
	};
	var ClientUIBehaviour = {
		formData: null,
		onAddMessageLink: function (e) {
			e.preventDefault();
			e.stopPropagation();
			var link = prompt("Подойдёт любая абсолютная ссылка", "http://www.websiteforge.com/");
			if (link && URL_REGEXP.test(link)) {
				ClientUIBehaviour.formData.links.push(link);
				GUI.addMessageLink(link);
			} else {
				alert('Ссылка не добавлена');
			}
			return false;
		},
		onAddMessageYoutube: function (e) {
			e.preventDefault();
			e.stopPropagation();
			var link = prompt("Абсолютная ссылка на embedded youtube видео", "http://www.youtube.com/embed/dBfI1-V7Hmo");
			if (link && YOUTUBE_REGEXP.test(link)) {
				ClientUIBehaviour.formData.youtubes.push(link);
				GUI.addYoutubeLink(link);
			} else {
				alert('Видео не добавлено. Нужна ссылка вида http://www.youtube.com/embed/{VIDEO_ID}');
			}
			return false;
		},
		onAddMessageFile: function (e) {
			e.preventDefault();
			e.stopPropagation();
			var totalFileSize = 0;
			var isValidFiles = function (files) {
				var isValidFileType = function (file) {
					return Utils.inArray(file.type, [
						'image/jpeg',
						'image/png',
						'image/gif'
					]);
				};
				var isTooBigFile = function (file) {
					totalFileSize += file.size;
					return file.size > 8000;
				};
				if (files != undefined) {
					for (var i = 0; i < files.length; i++) {
						var file = files[i];
						if (!isValidFileType(file)) {
							return false;
						}
						if (isTooBigFile(file)) {
							return false;
						}
					}
					return true;
				}
				return false;
			};
			var renderFiles = function (files) {
				var renderFile = function (file) {
					var reader = new FileReader();
					reader.onload = function (event) {
						var image = document.createElement('img');
						image.src = event.target.result;
						GUI.addImage(image);
						ClientUIBehaviour.formData.files.push(event.target.result);
					};
					reader.readAsDataURL(file);
				};
				for (var i = 0; i < files.length; i++) {
					renderFile(files[i]);
				}
			};
			var files = e.target.files;
			if (isValidFiles(files) && totalFileSize < 8000) {
				renderFiles(files);
			} else {
				document.getElementById('add-file').value = '';
				alert('Должны быть изображения jpeg, png или gif не более 8КБ вместе взятые.');
			}
			return false;
		},
		onSendMessage: function (e) {
			e.preventDefault();
			e.stopPropagation();
			ClientUIBehaviour.formData.messageText = document.getElementById('message').value;
			var message = ClientUIBehaviour.formData.getMessage();
			if (Utils.inArray(message.message, [null, '', undefined])
				&& message.files.length == 0
				&& message.links.length == 0
				&& message.youtubes.length == 0
			) {
				alert('Сообщение должно содержать хоть что-то');
				return false;
			}
			ServerFasade.postMessage(message);
			GUI.crearPreviewMessageContainer();
			GUI.clearControls();
			ClientUIBehaviour.formData.clear();
			return false;
		},
		onLikeMessage: function (e) {
			e.preventDefault();
			e.stopPropagation();
			var messageId = e.target.getAttribute('data-message');
			ServerFasade.likeMessage({
				user: User.getName(),
				messageId: messageId
			});
			return false;
		},
		onDeleteMessage: function (e) {
			e.preventDefault();
			e.stopPropagation();
			var messageId = e.target.getAttribute('data-message');
			ServerFasade.deleteMessage({
				user: User.getName(),
				messageId: messageId
			});
			return false;
		}
	};
	var MessageBox = {
		container: function () {
			return document.getElementById('message-container');
		},
		pushMessage: function (html, callback) {
			var container = MessageBox.container();
			var elem = document.createElement('li');
			elem.insertBefore(html, elem.firstChild);
			container.insertBefore(elem, container.firstChild);
			if (callback) {
				callback();
			}
		}
	};
	var ResponseHandler = {
		postMessage: function (data) {
			var createTextBlock = function (data) {
				var div = document.createElement('div');
				var content = data.createdAt + ' :: ' + data.user + ': ';
				if (!Utils.inArray(data.messageText, ['', null, undefined])) {
					content += data.messageText;
				}
				div.innerHTML = content;
				return div;
			};
			var createControlsBlock = function (data) {
				var div = document.createElement('div');
				if (data.user == User.getName()) {
					var button = document.createElement('button');
					button.setAttribute('class', 'delete-message');
					button.setAttribute('data-message', data.messageId);
					button.onclick = ClientUIBehaviour.onDeleteMessage;
					button.innerHTML = 'Удалить';
					div.insertBefore(button, div.firstChild);
				}
				var likeButton = document.createElement('button');
				likeButton.setAttribute('class', 'like-message');
				likeButton.setAttribute('data-message', data.messageId);
				likeButton.onclick = ClientUIBehaviour.onLikeMessage;
				likeButton.innerHTML = 'Нравится!';
				div.insertBefore(likeButton, div.firstChild);
				var likeCount = document.createElement('div');
				likeCount.setAttribute('class', 'like-count');
				likeCount.innerHTML = 'Понравилось: ' + data.likes;
				div.insertBefore(likeCount, div.firstChild);
				return div;
			};
			var el = document.createElement('div');
			el.id = 'message' + data.messageId;
			el.setAttribute('style', 'border: 1px solid #000');
			el.insertBefore(createControlsBlock(data), el.firstChild);
			if (data.youtubes.length > 0) {
				var div = document.createElement('div');
				div.innerHTML = 'Видео:<br/>';
				var createYoutubeElement = function (link) {
					var elem = document.createElement('iframe');
					elem.width = 560;
					elem.height = 315;
					elem.src = link;
					elem.frameborder = 0;
					elem.allowfullscreen = null;
					return elem;
				};
				for (var i = 0; i < data.youtubes.length; i++) {
					var link = data.youtubes[i];
					div.appendChild(createYoutubeElement(link));
					div.appendChild(document.createElement('br'));
				}
				el.insertBefore(div, el.firstChild);
			}
			if (data.files.length > 0) {
				var divFile = document.createElement('div');
				divFile.innerHTML = 'Картинки:<br/>';
				var createFileElement = function (file) {
					var image = document.createElement('img');
					image.src = file;
					return image;
				}
				for (var k = 0; k < data.files.length; k++) {
					divFile.appendChild(createFileElement(data.files[k]));
					divFile.appendChild(document.createElement('br'));
				}
				el.insertBefore(divFile, el.firstChild);
			}
			if (data.links.length > 0) {
				var divLink = document.createElement('div');
				divLink.innerHTML = 'Ссылки:<br/>';
				var createLinkElement = function (link) {
					var a = document.createElement('a');
					a.innerHTML = link;
					a.href = link;
					return a;
				}
				for (var j = 0; j < data.links.length; j++) {
					divLink.appendChild(createLinkElement(data.links[j]));
					divLink.appendChild(document.createElement('br'));
				}
				el.insertBefore(divLink, el.firstChild);
			}
			el.insertBefore(createTextBlock(data), el.firstChild);
			MessageBox.pushMessage(el);
		},
		deleteMessage: function (data) {
			switch (data.status) {
				case 'Ok':
					var $message = document.getElementById('message' + data.messageId);
					$message.parentNode.remove();
					break;
				case 'Error':
				default :
					break;
			}
		},
		likeMessage: function (data) {
			switch (data.info) {
				case 'Added':
					var $message = document.getElementById('message' + data.messageId);
					$message.querySelector('.like-count').innerHTML = 'Понравилось: ' + data.likes;
					if (data.user == User.getName()) {
						$message.getElementsByClassName('like-message').remove();
					}
					break;
				case 'AlreadyExist':
				case 'Error':
				default :
					break;
			}
		}
	};
	var ChatInitializer = {
		init: function () {
			var webSocket = new Socket('127.0.0.1', '8000', ResponseHandler);
			ServerFasade.setSocket(webSocket);
			ClientUIBehaviour.formData = new MessageFormDataContainer();
			document.getElementById('add-link').onclick = ClientUIBehaviour.onAddMessageLink;
			document.getElementById('add-file').onchange = ClientUIBehaviour.onAddMessageFile;
			document.getElementById('add-youtube').onclick = ClientUIBehaviour.onAddMessageYoutube;
			document.getElementById('form').onsubmit = ClientUIBehaviour.onSendMessage;
			document.getElementsByClassName('like-message').onclick = ClientUIBehaviour.onLikeMessage;
			document.getElementsByClassName('delete-message').onclick = ClientUIBehaviour.onDeleteMessage;
		}
	};
	window.onload = function () {
		ChatInitializer.init();
	}
})(document);