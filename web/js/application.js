var Socket = (function () {
    function Socket(host, port) {
        this.connectionString = 'ws://' + host + ':' + port;
    }
    Socket.prototype.setMessageHandler = function (handler) {
        this.onMessageHandler = handler;
    };
    Socket.prototype.close = function () {
        this.socket.close();
    };
    Socket.prototype.connect = function () {
        if (!this.onMessageHandler) {
            throw new Error('Message handler is not defined');
        }
        this.socket = new WebSocket(this.connectionString);
        if (this.socket) {
            this.socket.onmessage = this.onMessageHandler;
            this.socket.onopen = function () {
                console.log('Соединение установлено');
            };
            this.socket.onclose = function (e) {
                if (e.wasClean) {
                    console.log('Соединение закрыто чисто');
                }
                else {
                    console.log('Обрыв соединения');
                }
                console.log('Код: ' + event.code + ', причина: ' + event.reason);
            };
            this.socket.onerror = function (e) {
                console.log('Ошибка: ' + e.error);
            };
        }
    };
    Socket.prototype.send = function (data) {
        this.socket.send(data);
    };
    return Socket;
})();
var ChatServerMessage = (function () {
    function ChatServerMessage(commandName, data) {
        this.command = commandName;
        this.data = data;
    }
    ChatServerMessage.prototype.getCommand = function () {
        return undefined;
    };
    ChatServerMessage.prototype.getData = function () {
        return undefined;
    };
    ChatServerMessage.fromMessageEvent = function (ev) {
        var data = JSON.parse(ev.data);
        return new ChatServerMessage(data.command, data.data);
    };
    return ChatServerMessage;
})();
var ChatServer = (function () {
    function ChatServer(socket) {
        this.listeners = [];
        this.socket = socket;
    }
    ChatServer.prototype.start = function () {
        var that = this;
        this.socket.connect();
        this.socket.setMessageHandler(function (ev) {
            var serverMessage = ChatServerMessage.fromMessageEvent(ev);
            that.listeners.forEach(function (listener) {
                that.socket.send(listener.onServerMessage(serverMessage));
            });
        });
    };
    ChatServer.prototype.stop = function () {
        this.socket.close();
    };
    ChatServer.prototype.addListener = function (listener) {
        this.listeners.push(listener);
        listener.setServer(this);
    };
    return ChatServer;
})();
var socket = new Socket('127.0.0.1', '8000');
var server = new ChatServer(socket);
server.addListener();
try {
    server.start();
}
catch (e) {
    server.stop();
    console.log(e);
}
//# sourceMappingURL=application.js.map