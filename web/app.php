<?php

include_once '../vendor/autoload.php';

define('APP_DIR', __DIR__ . '/../');

$app = new \KustovVitalik\Chat\Chat();

$app->run();