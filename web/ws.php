#!/usr/bin/env php
<?php

define('APP_DIR', __DIR__ . '/../');

include_once APP_DIR . 'vendor/autoload.php';

$app = new \KustovVitalik\Chat\Chat();
$app->container->get('ws_server')->start();