<?php
/**
 * Created by PhpStorm.
 * Date: 14.07.2015
 * @author Vitaly
 */

define('ROOT_PATH', basename(basename(__DIR__)));
define('APP_DIR', __DIR__ . '/../');

include_once ROOT_PATH . '/../vendor/autoload.php';
include_once ROOT_PATH . '/../vendor/doctrine/common/lib/Doctrine/Common/ClassLoader.php';

use Doctrine\Common\ClassLoader;
use Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand;
use Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand;
use Doctrine\DBAL\Migrations\Tools\Console\Command\GenerateCommand;
use Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand;
use Doctrine\DBAL\Migrations\Tools\Console\Command\StatusCommand;
use Doctrine\DBAL\Migrations\Tools\Console\Command\VersionCommand;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Version;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Helper\DialogHelper;

$classLoader = new ClassLoader('Doctrine\DBAL\Migrations', ROOT_PATH . '/../vendor/doctrine/migrations/lib');
$classLoader->register();

$cli = new Application('Doctrine command line interface', Version::VERSION);
$cli->setCatchExceptions(true);

$commands = [
	new DiffCommand(),
    new ExecuteCommand(),
    new GenerateCommand(),
    new MigrateCommand(),
    new StatusCommand(),
    new VersionCommand()
];

$app = new \KustovVitalik\Chat\Chat();
$em = $app->container->get('entity_manager');

$helperSet = ConsoleRunner::createHelperSet($em);
$helperSet->set(new DialogHelper());

return $helperSet;