# README #

### Для кого? ###

* Zennex

### Как запустить? ###

* **cd /var/www** 
* **git clone git@bitbucket.org:vitalik-kustov/websocket-chat.git** 
* **cd websocket-chat**   
Linux: 
* **curl -sS https://getcomposer.org/installer | php**   
Windows: 
* **php -r "readfile('https://getcomposer.org/installer');" | php** 
* **php composer.phar install -o** 
* Открыть config/services.yml и задать настройки для базы в блоке parameters.db, создать базу. 
* Выполнить **vendor/bin/doctrine migrations:migrate** 
* Настроить виртуальный хост для apache

```
#!config

<VirtualHost chat.loc:80>
   DocumentRoot "/var/www/websocket-chat/web"
    ServerName chat.loc
    ErrorLog "logs/chat-error.log"
   CustomLog "logs/chat-access.log" combined	
	<Directory "/var/www/websocket-chat/web">
		AllowOverride All
		Order allow,deny
		Allow from all
	</Directory>	
</VirtualHost>
```

* Добавить в hosts файл 127.0.0.1 chat.loc 
* Запустить apache 
* Выполнить **php web/ws.php** 
* Чат доступен [http://chat.loc/](http://chat.loc/)  